var express = require('express');
var fs = require('fs');
var Tools = require('./libs/Tools');

var app = express();

// Order that matters
require('dotenv').config({silent: true});
require('./config/express')(app);
require('./config/routes')(app);

app.listen(app.get('port'), function onStart(err) {
  if (err) {
    console.log(err);
  }

  console.log('Listening on port %s. Open up http://localhost:%s/ in your browser.', app.get('port'), app.get('port'));
});
