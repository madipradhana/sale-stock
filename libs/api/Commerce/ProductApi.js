var axios = require('axios');
var Tools = require('../../Tools');


function productList() {
  var baseUrl = 'http://swapi.co/api';
  var url = baseUrl + '/films';

  return axios.get(url);
}

function getProduct(id) {
  var baseUrl = 'http://swapi.co/api/films';
  var url = `${baseUrl}/${id}`;
  debugger;
  return axios.get(url);
}

module.exports = {
  productList: productList,
  getProduct: getProduct
};
