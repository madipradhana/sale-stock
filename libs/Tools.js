var _ = require('lodash');
var env = process.env;


function isRunningInNode() {
  return (typeof module !== 'undefined' &&
    module.exports &&
    typeof window === 'undefined');
}

function baseUrl() {
  if (!isRunningInNode()) {
    return window.baseUrl;
  }

  return env.BASE_URL;
}


function originUrl() {
  if (isRunningInNode()) {
    return null;
  }

  if (window.location.origin) {
    return window.location.origin;
  }

  return (window.location.protocol + '//' +
    window.location.hostname +
    (window.location.port ? ':' + window.location.port : ''));
}

module.exports = {
  baseUrl: baseUrl,
  originUrl: originUrl
};
