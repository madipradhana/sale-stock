import AppAction from 'actions/App/AppAction';

class AppStore {

  constructor() {
    this.bindListeners({
      changeHeader: AppAction.CHANGE_HEADER
    });

    this.state = {
      header: {
        show: true,
        navigation: true,
        search: true,
        logo: true,
        logoText: false
      },
      current_locale: null
    };
  }

  changeHeader(params) {
    this.setState({
      header: params
    });
  }
}

export default alt.createStore(AppStore, 'AppStore');
