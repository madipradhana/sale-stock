import ProductAction from 'actions/Commerce/ProductAction';

class ProductStore {

  constructor() {
    this.bindListeners({
      fetchProducts: ProductAction.FETCH_PRODUCTS,
      updateProducts: ProductAction.UPDATE_PRODUCTS,
      getProduct: ProductAction.GET_PRODUCT,
      updateProduct: ProductAction.UPDATE_PRODUCT
    });

    this.state = {
      loading: false,
      products: []
    };
  }

  fetchProducts() {
    this.setState({loading: true});
  }

  updateProducts(data) {
    const products = data.results;
    this.setState({
      loading: false,
      products: products
    });
  }

  getProduct() {
    this.setState({loading: true});
  }

  updateProduct(data) {
    const product = data;
    this.setState({
      loading: false,
      product: product
    });
  }
}

export default alt.createStore(ProductStore, 'ProductStore');
