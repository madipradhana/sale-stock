import { renderToString } from 'react-dom/server';
import { RoutingContext, match } from 'react-router';
import { createLocation } from 'history';
import Iso from 'iso';

import alt from 'altInstance';
import routes from 'routes.jsx';
import html from '../views/base.html';

const renderMarkup = (alt, state, req, res) => {
  let markup, content;
  const location = new createLocation(req.url);
  alt.bootstrap(state);

  match({ routes, location }, (error, redirectLocation, renderProps) => {
    if (redirectLocation) {
      return res.redirect(301, redirectLocation.pathname + redirectLocation.search);
    } else if (error) {
      return res.status(500).send(error.message);
    } else if (renderProps == null) {
      return res.status(404).send('Not found')
    } else {
      content = renderToString(<RoutingContext {...renderProps} />);
      markup = Iso.render(content, alt.flush());
    }
  });

  return markup;
};

export default function render(state, req, res) {
  const markup = renderMarkup(alt, state, req, res);
  return html.replace('CONTENT', markup);
};
