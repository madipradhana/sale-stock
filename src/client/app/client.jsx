import Iso from 'iso';
import { Router, browserHistory } from 'react-router';

import alt from 'altInstance';
import routes from 'routes.jsx';

Iso.bootstrap((state, _, container) => {
  function run() {
    alt.bootstrap(state);
    ReactDOM.render(<Router history={browserHistory} children={routes} />, container);
  }

  run();
});
