import AppAction from 'actions/App/AppAction';

export default class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    AppAction.changeHeader({
      search: false
    });
  }

  render() {
    return (
      <section>
        This is Home!!!
      </section>
    );
  }
}

Home.propTypes = {
  ProductStore: React.PropTypes.object,
  children: React.PropTypes.object
};
