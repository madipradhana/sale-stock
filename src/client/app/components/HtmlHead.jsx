import Helmet from 'react-helmet';
import {
  config,
  siteName,
  defaultTitle
} from 'helmet.js';

// Server Rendering only
export default class HtmlHead extends React.Component {

  constructor(props) {
    super(props);

    this.displayName = 'HtmlHead';
  }

  render() {
    const { url } = this.props.req;
    const originUrl = `${siteName}${url}`;

    let helmet;
    let titleName;
    let pageDescription = 'This is page description!';
    let social;
    let pageIndex = 'index';

    helmet = config(
      originUrl,
      titleName,
      pageDescription,
      social,
      pageIndex
    );

    return (
      <Helmet
        title={helmet.title}
        meta={helmet.meta}
        link={helmet.link}
        script={helmet.script} />
    );
  }

}

HtmlHead.propTypes = {
  req: React.PropTypes.object,
  res: React.PropTypes.object
};
