import {Thumbnail} from 'react-bootstrap';
import styles from './style.css';

const ProductCard = (props) => {
  const _handleClick = (event) => {
    event.preventDefault();
    props.onCardClick(props.productId);
  };

  let productImage = {
    backgroundImage: 'url(' + props.thumbnail + ')'
  };

  return (
    <Thumbnail bsClass={`${styles.card}`} onClick={_handleClick}>
      <div className={`${styles.wrapper}`}>
        <div className={`${styles.image}`} style={ props.thumbnail ? productImage : null } />
        <div className={`${styles.description}`}>
          <h3 className={`${styles.title}`}>{props.title}</h3>
          <div className={`${styles.body}`}>{props.body}</div>
        </div>
      </div>
    </Thumbnail>
  );
};

ProductCard.propTypes = {
  productId: React.PropTypes.string,
  title: React.PropTypes.string,
  body: React.PropTypes.object,
  thumbnail: React.PropTypes.string,
  onCardClick: React.PropTypes.func
};


export default ProductCard;
