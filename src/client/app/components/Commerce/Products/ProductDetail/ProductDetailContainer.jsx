import ProductAction from 'actions/Commerce/ProductAction';

import ProductDetail from './ProductDetail';
import CommentList from '../../../../shared/Comments/CommentList';
import Loading from '../../../../shared/Loading';

import styles from './style.css';

export default class ProductDetailContainer extends React.Component {
  constructor(props) {
    super(props);
    const loadingState = this.props.location.state ? false : true;
    this.state = {
      product: this.props.location.state || {},
      loading: loadingState
    };
  }

  componentDidMount() {
    if (_.isEmpty(this.state.product)) {
      ProductAction.getProduct(this.props.params.id);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.ProductStore.loading === true) {
      return;
    }

    this.setState({
      product: nextProps.location.state || nextProps.ProductStore.product,
      loading: false
    });
  }

  render() {
    let Content;
    if (!_.isEmpty(this.state.product) ) {
      const props = this.props.location.state ? this.props.location.state : this.state.product;
      let { title, director, producer } = props;
      let id = props['episode_id'];
      let description = (
          <div>
            <div>Director: { director }</div>
            <div>Producer: { producer }</div>
            <p>{ props['opening_crawl'] }</p>
          </div>
        );

      Content = (
        <div className={styles.container}>
          <ProductDetail productId={id} title={title} description={description} />
          <CommentList {...this.props} />
        </div>
      );
    }

    return (
      <div className={styles.root}>
        <div role="main" className={styles.container}>
          { this.state.loading ? <Loading/> : Content }
        </div>
      </div>
    );
  }
}

ProductDetailContainer.propTypes = {
  ProductStore: React.PropTypes.object,
  params: React.PropTypes.object,
  location: React.PropTypes.object.isRequired
};
