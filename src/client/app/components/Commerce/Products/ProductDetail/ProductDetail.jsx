import styles from './style.css';

const ProductDetail = (props) => {
  return (
    <article>
      <h1>{props.title}</h1>
      {props.description}
    </article>
  );
};

ProductDetail.propTypes = {
  title: React.PropTypes.string.isRequired,
  description: React.PropTypes.object.isRequired
};


export default ProductDetail;
