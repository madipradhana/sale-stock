import {Grid, Row, Col} from 'react-bootstrap';
import { Router } from 'react-router';

import ProductCard from '../ProductCard';

import styles from './style.css';

export default class ProductList extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    const Content = this.props.products.map((product) => {
      const id = product['episode_id'].toString();
      const releaseDate = product['release_date'];
      const {title, producer, director, characters, posters} = product;

      const body = (
          <div>
            <div className={styles.description}>{product['opening_crawl']}</div>
            <div className={styles.release}>Release: {product['release_date']}</div>
          </div>
        );

      return (
          <Col key={id} xs={12} sm={6} md={4} className={styles['card-column-container']}>
            <ProductCard
              productId={id}
              title={title}
              body={body}
              thumbnail={posters ? posters.original : ''}
              onCardClick={this._handleCardClick.bind(this)} />
          </Col>
        );
    });

    return (
      <Grid>
        <Row>
          { Content }
        </Row>
      </Grid>
    );
  }

  _handleCardClick(id) {
    const foundItemOnIndex = _.findIndex(this.props.products, {episode_id: parseInt(id, 10)});
    this.context.router.push({
      pathname: `/product/${id}`,
      state: this.props.products[foundItemOnIndex]
    });
  }
}

ProductList.propTypes = {
  products: React.PropTypes.array
};

ProductList.contextTypes = {
  router: React.PropTypes.object
};
