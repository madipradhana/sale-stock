import ProductAction from 'actions/Commerce/ProductAction';

import ProductList from './ProductList';
import Loading from '../../../../shared/Loading';

import styles from './style.css';

export default class ProductListContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: this.props.ProductStore.products || [],
      loading: true
    };
  }

  componentDidMount() {
    ProductAction.fetchProducts();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.ProductStore.loading === true) {
      return;
    }

    this.setState({
      products: nextProps.ProductStore.products,
      loading: nextProps.ProductStore.loading
    });
  }

  render() {
    const Content = (this.state.products.length > 0 ? <ProductList products={this.state.products} /> : false);
    return (
      <div role="main" className={styles.container}>
        { Content }
        { this.state.loading ? <Loading/> : false }
      </div>
    );
  }
}

ProductListContainer.propTypes = {
  ProductStore: React.PropTypes.object
};
