import AltContainer from 'alt-container';

import AppStore from 'stores/App/AppStore';
import ProductStore from 'stores/Commerce/ProductStore';

import Header from 'shared/Header';
import FloatBox from 'shared/FloatBox';
import 'assets/css/app';

export default class App extends React.Component {
  render() {
    const { children, main, floatbox } = this.props;
    const Content = children ? children : main;
    let FloatBoxComponent = <div/>;
    if (floatbox) {
      FloatBoxComponent = (
        <FloatBox {...this.props}>
          {floatbox}
        </FloatBox>);
    }

    return (
      <div id="wrapper">
        <AltContainer
          stores={{
            AppStore,
            ProductStore
          }}
        >
          <Header {...this.props} />
          { FloatBoxComponent }
          { Content }
        </AltContainer>
      </div>
    );
  }
}


App.propTypes = {
  children: React.PropTypes.object,
  main: React.PropTypes.object,
  floatbox: React.PropTypes.object
};
