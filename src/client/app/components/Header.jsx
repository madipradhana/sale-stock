import { renderToString } from 'react-dom/server';
import Helmet from 'react-helmet';
import HtmlHead from 'components/HtmlHead';


export default function render(req, res) {
  renderToString(<HtmlHead req={req} res={res}/>);
  return Helmet.rewind();
};
