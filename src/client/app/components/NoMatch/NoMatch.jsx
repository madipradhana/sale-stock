import styles from './style.css';

export default class NoMatch extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={ styles.root }>
        <h1>404</h1>
      </div>
    );
  }
}
