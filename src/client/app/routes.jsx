import { Route, IndexRoute } from 'react-router';
import App from 'components/App';
import Home from 'components/Homes/Home';
import ProductListContainer from 'components/Commerce/Products/ProductList';
import ProductDetailContainer from 'components/Commerce/Products/ProductDetail';
import NoMatch from 'components/NoMatch';


let requireFloatboxConstruct = (nextState, replace) => {
  const defaultCartState = { title: 'My Cart', data: [] };
  if (nextState.location && nextState.location.state === null) {
    nextState.location.state = defaultCartState;
  }
};

export default (
  <Route path="/" component={ App }>
    <IndexRoute component={ProductListContainer} />
    <Route path="store" component={ ProductListContainer } />
    <Route path="product/:id" component={ ProductDetailContainer } />
    <Route path="*" component={ NoMatch } />
  </Route>
);
