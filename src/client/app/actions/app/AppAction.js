class AppAction {
  changeHeader(params) {
    this.dispatch(params);
  }
}

export default alt.createActions(AppAction);
