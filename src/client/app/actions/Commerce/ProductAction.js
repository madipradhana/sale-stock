class ProductAction {
  fetchProducts() {
    this.dispatch();

    axios.get('/data')
      .then((response) => {
        if (response.status === 200) {
          this.actions.updateProducts(response.data);
        } else {
          console.log('an error occured');
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  updateProducts(data) {
    this.dispatch(data);
  }

  getProduct(id) {
    this.dispatch();

    axios.get('/data/' + id)
      .then((response) => {
        if (response.status === 200) {
          this.actions.updateProduct(response.data);
        } else {
          console.log('an error occured');
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }

  updateProduct(data) {
    this.dispatch(data);
  }

}

export default alt.createActions(ProductAction);
