import favicon from 'assets/images/favicon.png';
import hrCover from 'assets/images/cover.jpg';

export const defaultTitle = 'TopTal';
export const siteName = ''; // without slash '/'
export const imageWidth = '300';
export const imageHeight = '300';


export function config(originUrl, currentLocale, title, description, urlHref, socialArrays = [], robots = 'index', keywords = []) {
  let defaultConfig = {
    title: title,
    link: [
      { 'rel': 'icon', 'href': favicon },
      { 'rel': 'author', 'href': `${siteName}/author.txt`, 'type': 'text/plain' }
    ],
    meta: [
      { 'charset': 'utf-8' },
      { 'http-equiv': 'X-UA-Compatible', 'content': 'IE=edge' },
      { 'name': 'description', 'content': description },
      { 'name': 'viewport', 'content': 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' },
      { 'name': 'mobile-web-app-capable', 'content': 'yes' },
      { 'name': 'apple-mobile-web-app-capable', 'content': 'yes' },
      { 'name': 'apple-mobile-web-app-status-bar-style', 'content': 'black' },
      { 'name': 'apple-mobile-web-app-title', 'content': title }
    ],
    script: [
    ]
  };

 // defaultConfig.meta = defaultConfig.meta.concat(websiteInfo(currentLocale, originUrl));

  if (keywords.length) {
    defaultConfig.meta = defaultConfig.meta.concat({ 'name': 'keywords', 'content': keywords.join(',') });
  }

  if (!socialArrays.length) {
    return defaultConfig;
  }

  return defaultConfig;
}
