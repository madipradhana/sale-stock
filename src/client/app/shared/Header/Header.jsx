import {Nav, Navbar, NavDropdown, NavItem, MenuItem, Input, Button} from 'react-bootstrap';
import { Link } from 'react-router';

export default class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let brand = this.props.brand;
    const navbarHeader = (
      <Navbar.Header>
        <Navbar.Brand>
          <Link to={ brand.url }>{ brand.name }</Link>
        </Navbar.Brand>
      </Navbar.Header>
    );

    let navbarNavigation = this._parseNavigation();
    return (
      <Navbar fixedTop={this.props.fixedTop}>
        { navbarHeader }
        { navbarNavigation }
      </Navbar>
    );
  }

  _parseNavigation() {
    let key = 1;

    //get navigation node:
    let NavigationItems = _.map(this.props.navitems, (item) => {
      let NavigationNode;
      //NavItem
      if (item.type === 'item') {
        NavigationNode = (
          <Navbar.Text key={key} eventKey={key}>
            <Link to={ item.url }>{ item.name }</Link>
          </Navbar.Text>
        );
      } else if (item.type === 'dropdown') {
        let subKey = 1;
        NavigationNode = _.map(item.nodes, (node) => {
          let DropDownNode;
          if (node.type === 'item') {
            let tempKey = key + '.' + subKey;
            DropDownNode = (
              <MenuItem eventKey={parseFloat(tempKey)}><Link to={ item.url }>{item.name}</Link></MenuItem>
            );
          }
          subKey += 1;
          return DropDownNode;
        });

        if (NavigationNode) {
          let DropdownNavigationNode = (
            <NavDropdown eventKey={parseInt(key, 10)} title="Dropdown" id="basic-nav-dropdown">
              {NavigationNode}
            </NavDropdown>
          );
          NavigationNode = DropdownNavigationNode;
        }
      }
      key += 1;
      return NavigationNode;
    });

    return (
      <Navbar.Collapse>
        <Nav>
          { NavigationItems }
        </Nav>
        { this.props.children }
      </Navbar.Collapse>
    );
  }
}

Header.propTypes = {
  headerSetting: React.PropTypes.object,
  brand: React.PropTypes.object,
  fixedTop: React.PropTypes.bool,
  navitems: React.PropTypes.array,
  children: React.PropTypes.object
};
