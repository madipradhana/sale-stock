import {Nav, Navbar, NavItem} from 'react-bootstrap';
import Header from './Header';

import styles from './style.css';

export default class HeaderContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      headerSetting: this.props.AppStore.header
    };
  }

  render() {
    return (
      <section>
        <Header headerSetting={this.state.headerSetting} brand={ this._brand() } fixedTop navitems={ this._navigation() } />
      </section>
    );
  }

  _brand() {
    const brand = {
      image: null,
      name: 'MA',
      url: '/'
    };

    return brand;
  }

  _navigation() {
    const navigation = [
      {
        type: 'item',
        name: 'Store',
        url: '/store'
      },
      {
        type: 'item',
        name: 'About',
        url: 'https://www.linkedin.com/in/mirzaadipradhana'
      }
    ];

    return navigation;
  }
}


HeaderContainer.propTypes = {
  AppStore: React.PropTypes.object,
  history: React.PropTypes.object
};

