import styles from './style.css';

const Comment = (props) => {
  return (
    <div className={styles.root}>
      <h2 className={styles.author}>{props.author}</h2>
      <p className={styles.comment}>{props.children}</p>
    </div>
  );
};

Comment.propTypes = {
  children: React.PropTypes.string,
  author: React.PropTypes.string
};


export default Comment;
