import Firebase from 'firebase';

import CommentList from './CommentList';
import CommentForm from '../CommentForm';
import Loading from '../../Loading';

import styles from './style.css';

export default class CommentListContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      comments: [],
      loading: true
    };
  }
  componentWillMount() {
    let comments = this.state.comments;
    // const firebaseUrl = 'https://blistering-heat-7793.firebaseio.com/swapi/';
    const firebaseUrl = 'https://lf4cttz6fs0.firebaseio-demo.com/';
    this.firebaseRef = new Firebase(firebaseUrl);

    this.firebaseRef.on('child_added', function(dataSnapshot) {
      const payload = dataSnapshot.val();
      const author = payload.name;
      const message = payload.text;
      let comment = {
        author: payload.name,
        comment: payload.text
      };

      comments.push(comment);
      this.setState({
        comments: comments,
        loading: false
      });
    }.bind(this));
  }

  componentWillUnmount() {
    this.firebaseRef.off();
  }

  render() {
    const Comment = (this.state.comments.length > 0 ? <CommentList comments={this.state.comments} /> : false);
    const Content = (
      <div className={styles.root}>
        <h1>Comments</h1>
        { this.state.loading ? <Loading/> : Comment }
      </div>
    );
    return Content;
  }
}

CommentListContainer.propTypes = {
  params: React.PropTypes.object
};
