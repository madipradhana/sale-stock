import styles from './style.css';
import Comment from '../Comment';

export default class CommentList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      comments: this.props.comments || [],
      loading: true
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      comments: nextProps.comments,
      loading: false
    });
  }

  render() {
    let Content = <div className={styles.empty}>No Comments</div>;
    let commentNodes = this.state.comments.map( (value, index) => {
      return <Comment key={index} author={value.author}>{value.comment}</Comment>;
    });

    if (commentNodes) {
      Content = commentNodes;
    }

    const RenderedComponent = (
      <div className={styles.comments}>
        { Content }
      </div>
    );

    return RenderedComponent;
  }
}

CommentList.propTypes = {
  comments: React.PropTypes.array
};
