import {FormGroup, FormControl, ControlLabel, Button} from 'react-bootstrap';

import styles from './style.css';

export default class CommentForm extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <form className="commentForm">
        <ControlLabel>Name</ControlLabel>
        <ControlLabel>Comment</ControlLabel>
        <Button onClick={this._handleSubmit}>Post!</Button>
      </form>
    );
  }

  _handleSubmit(event) {
    event.preventDefault();
    let author = this.refs.author.value.trim();
    let text = this.refs.text.value.trim();
    this.props.onCommentSubmit({author: author, text: text});
    this.refs.author.value = '';
    this.refs.text.value = '';
  }
}

CommentForm.propTypes = {
  onCommentSubmit: React.PropTypes.func,
  params: React.PropTypes.object
};

