import styles from './style.css';

const Loading = (props) => {
  return (
		<div className={styles.loading} />
  );
};

Loading.propTypes = {
  percent: React.PropTypes.number
};


export default Loading;
