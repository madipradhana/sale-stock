import { Router } from 'react-router';

import styles from './style.css';

export default class FloatBox extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const rootStyle = styles['root-overlay'];
    const closeButtonStyle = styles['close-button'];

    const title = (this.props.location.state && this.props.location.state.title ? this.props.location.state.title : (this.props.title || ''));
    const data = (this.props.location.state && this.props.location.state.data ? this.props.location.state.data : this.props.data);

    const Content = (
      <div className="floatbox-wrapper">
        <div className={ styles.header }>
          <h3>{ title }</h3>
          <div className={`glyphicon glyphicon-remove ${closeButtonStyle}`} onClick={this._handleCloseButton.bind(this)} />
        </div>
        <div className={ styles.body }>
          { React.cloneElement(this.props.children, { ...this.props })}
        </div>
      </div>
    );
    return (
      <div className={`${rootStyle}`}>
        <div className={`container ${styles.wrapper}`}>
          <div className={ styles.floatbox }>
            { Content }
          </div>
        </div>
      </div>
    );
  }

  _handleCloseButton() {
    this.context.router.push({
      pathname: '/'
    });
  }
}

FloatBox.propTypes = {
  history: React.PropTypes.object,
  children: React.PropTypes.object.isRequired,
  location: React.PropTypes.object.isRequired,
  title: React.PropTypes.string,
  data: React.PropTypes.object
};

FloatBox.contextTypes = {
  router: React.PropTypes.object.isRequired
};
