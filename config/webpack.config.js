require('dotenv').config({silent: true});
var path = require("path");
var webpack = require("webpack");
var config = require("./config");

var ExtractTextPlugin = require("extract-text-webpack-plugin");
var ManifestPlugin = require("webpack-manifest-plugin");

var assetsPath = path.join(__dirname, "..", "build", "assets");
var publicPath = "/assets/";
var appPath = path.join(__dirname, '..', process.env.APP_PATH);
var nodeModulesPath = path.resolve(__dirname, '..', process.env.NODE_MODULES_PATH);


var loaders = [
  {
    test: /\.js$|\.jsx$/,
    loaders: ["babel-loader?stage=0"],
    include: appPath
  },
  {
    test: /\.css$/,
    loader: ExtractTextPlugin.extract("style-loader", "css-loader?module&localIdentName=[name]--[local]--[hash:base64:5]" +
      "&includePaths[]=" + nodeModulesPath + "!postcss-loader")
  },
  {
    test: /\.scss$/,
    loaders: [
      'style-loader',
      'css-loader?modules&importLoaders=2&localIdentName=[name]--[local]--[hash:base64:5]',
      'postcss-loader',
      'sass-loader',
    ],
  },
  {
    test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
    loader: "url?limit=10000"
  },
  {
    test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
    loader: 'file'
  },
  { test: /\.html$/, loader: "html-loader" },
  { test: /\.jpg$|\.jpeg$|\.png$|\.svg|\.gif$/, loader: "file-loader" },
  { test: /\.json$/, loader: "json-loader" }
];

module.exports = [
  {
    name: "browser",
    context: appPath,
    entry: {
      app: "./client"
    },
    output: {
      path: assetsPath,
      filename: process.env.NODE_ENV === 'production' ? "[name]-[chunkgitsha].js" : "[name].js",
      chunkFilename: "[id].js",
      publicPath: publicPath
    },
    devtool: "source-map",
    module: {
      preLoaders: [{
        test: /\.js$|\.jsx$/,
        exclude: /node_modules/,
        loaders: ["eslint"]
      }],
      loaders: loaders
    },
    resolve: {
      extensions: config.WEBPACK_RESOLVE_EXTENSIONS,
      modulesDirectories: config.WEBPACK_RESOLVE_MODULES_DIR
    },
    plugins: [
      new ExtractTextPlugin(process.env.NODE_ENV === 'production' ? "app-[chunkgitsha].css" : "app.css"),
      new webpack.ProvidePlugin(config.WEBPACK_PLUGINS),
      new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.UglifyJsPlugin({
        compress: { warnings: false },
        comments: false
      }),
      new ManifestPlugin()
    ],
    postcss: function plugins(bundler) {
      return [
        require('postcss-import')({ addDependencyTo: bundler }),
        require('postcss-pxtorem')({
            root_value: 16,
            unit_precision: 5,
            prop_white_list: config.WHITE_LIST,
            selector_black_list: [],
            replace: true,
            media_query: false
        }),
        require('postcss-cssnext')({ autoprefixer: config.AUTOPREFIXER_BROWSERS }),
        require('postcss-mixins')(),
        require('postcss-nested')(),
        require('postcss-short')()
      ];
    }
  },
  {
    name: "server-side rendering",
    context: appPath,
    entry: {
      app: "./server",
      header: "./components/Header"
    },
    target: "node",
    output: {
      path: assetsPath,
      filename: "server/[name].js",
      publicPath: publicPath,
      libraryTarget: "commonjs2"
    },
    module: {
      loaders: loaders
    },
    resolve: {
      extensions: config.WEBPACK_RESOLVE_EXTENSIONS,
      modulesDirectories: config.WEBPACK_RESOLVE_MODULES_DIR
    },
    plugins: [
      new ExtractTextPlugin("server/app.css"),
      new webpack.ProvidePlugin(config.WEBPACK_PLUGINS),
      new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.UglifyJsPlugin({
        compress: { warnings: false },
        comments: false
      })
    ],
    postcss: function plugins(bundler) {
      return [
        require('postcss-import')({ addDependencyTo: bundler }),
        require('postcss-pxtorem')({
          root_value: 16,
          unit_precision: 5,
          prop_white_list: config.WHITE_LIST,
          selector_black_list: [],
          replace: true,
          media_query: false
        }),
        require('postcss-cssnext')({ autoprefixer: config.AUTOPREFIXER_BROWSERS }),
        require('postcss-mixins')(),
        require('postcss-nested')(),
        require('postcss-short')()
      ];
    }
  }

];