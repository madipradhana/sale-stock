/**
 * Routes for express app
 */

var _ = require('lodash');
var xssFilters = require('xss-filters');
var axios = require('axios');

var manifest = require('../build/assets/manifest.json');
var Header = require('../build/assets/server/header');
var App = require('../build/assets/server/app');

var ProductAPI = require('../libs/api/Commerce/ProductAPI');

module.exports = function(app) {

  function exceptions(req, res, next) {
    switch(req.url) {
      case '/assets':
      case '/config':
      case '/logout':
        return next('route');
    }

    return next();
  }

  // Healtycheck
  app.get('/ping', exceptions, function(req, res) {
    res.sendStatus(200);
  });

  app.get('/data', exceptions, function(req, res) {
    ProductAPI
      .productList()
      .then((response) => {
        res.status(response.status).send(response.data);
      })
      .catch((response) => {
        res.status(response.status).send(response.data);
      });
  });

  app.get('/data/:id', exceptions, function(req, res) {
    ProductAPI
      .getProduct(req.params.id)
      .then((response) => {
        res.status(response.status).send(response.data);
      })
      .catch((response) => {
        res.status(response.status).send(response.data);
      });
  });

  app.get('*', exceptions, function (req, res, next) {
    var header = Header(req, res);
    var html = App(JSON.stringify(res.locals.data || {}), req, res);

    html = html.replace('TITLE', header.title)
      .replace('META', header.meta)
      .replace('LINK', header.link)
      .replace('SCRIPT', header.script);

    html = html.replace('BUNDLE_CSS', '/assets/app.css').replace('BUNDLE_JS', '/assets/app.js');

    res.set({
      'Content-Type': 'text/html; charset=utf8'
    });

    return res.status(req.isRouteFound ? 200 : 404).end(html);
  });

};
