require('dotenv').config({silent: true});
var path = require("path");
var webpack = require("webpack");
var config = require("./config");

var host = "localhost";
var port = 8001;

var assetsPath = path.join(__dirname, '..', 'build/assets');
var publicPath = 'http://' + host + ":" + port + '/assets/';
var appPath = path.join(__dirname, '..', process.env.APP_PATH);
var nodeModulesPath = path.resolve(__dirname, '..', process.env.NODE_MODULES_PATH);

var loaders = [
  {
    test: /\.js$|\.jsx$/,
    loaders: ["react-hot-loader", "babel-loader?stage=0"],
    include: appPath
  },
  { test: /\.html$/, loader: "html-loader" },
  { test: /\.jpg$|\.jpeg$|\.png$|\.svg|\.gif$/, loader: "file-loader" },
  { test: /\.json$/, loader: "json-loader" }
];

module.exports = [
  {
    name: "browser",
    context: appPath,
    entry: {
      app:[
        'webpack-dev-server/client?http://' + host + ":" + port,
        'webpack/hot/only-dev-server',
        'font-awesome-loader',
        'bootstrap-loader',
        './client'
      ]
    },
    output: {
      path: assetsPath,
      filename: "[name].js",
      publicPath: publicPath
    },
    module: {
      preLoaders: [{
        test: /\.js$|\.jsx$/,
        exclude: /node_modules/,
        loaders: ["eslint"]
      }],
      loaders: loaders.concat([
        { test: /\.css$/,
          loader: 'style-loader!css-loader?modules&localIdentName=[name]--[local]--[hash:base64:5]&camelCase&includePaths[]=' + nodeModulesPath + '!postcss-loader'
        },
        {
          test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: "url-loader?limit=10000"
        },
        {
          test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
          loader: 'file-loader'
        }
      ])
    },
    resolve: {
      extensions: config.WEBPACK_RESOLVE_EXTENSIONS,
      modulesDirectories: config.WEBPACK_RESOLVE_MODULES_DIR
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.ProvidePlugin(config.WEBPACK_PLUGINS),
      new webpack.NoErrorsPlugin()
    ],
    postcss: function plugins(bundler) {
      return [
        require('postcss-import')({ addDependencyTo: bundler }),
        require('postcss-pxtorem')({
            root_value: 16,
            unit_precision: 5,
            prop_white_list: config.WHITE_LIST,
            selector_black_list: [],
            replace: true,
            media_query: false
        }),
        require('postcss-cssnext')({ autoprefixer: config.AUTOPREFIXER_BROWSERS }),
        require('postcss-mixins')(),
        require('postcss-nested')(),
        require('postcss-short')()
      ];
    }
  }, {
    name: "server-side rendering",
    context: appPath,
    entry: {
      app: "./server",
      header: "./components/Header"
    },
    target: "node",
    output: {
      path: assetsPath,
      filename: "server/[name].js",
      publicPath: publicPath,
      libraryTarget: "commonjs2"
    },
    externals: /^[a-z\-0-9]+$/,
    module: {
      loaders: loaders.concat([
          { test: /\.css$/,
            loader: 'css-loader/locals?modules&localIdentName=[name]--[local]--[hash:base64:5]&sourceMap' +
              '&includePaths[]=' + nodeModulesPath + '!postcss-loader'
          }
      ])
    },
    resolve: {
      extensions: config.WEBPACK_RESOLVE_EXTENSIONS,
      modulesDirectories: config.WEBPACK_RESOLVE_MODULES_DIR
    },
    plugins: [
      new webpack.ProvidePlugin(config.WEBPACK_PLUGINS)
    ],
    postcss: function plugins(bundler) {
      return [
        require('postcss-import')({ addDependencyTo: bundler }),
        require('postcss-pxtorem')({
            root_value: 16,
            unit_precision: 5,
            prop_white_list: config.WHITE_LIST,
            selector_black_list: [],
            replace: true,
            media_query: false
        }),
        require('postcss-cssnext')({ autoprefixer: config.AUTOPREFIXER_BROWSERS }),
        require('postcss-mixins')(),
        require('postcss-nested')(),
        require('postcss-short')()
      ];
    }
  }
];