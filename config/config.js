
const AUTOPREFIXER_BROWSERS = [
  'Android 2.3',
  'Android >= 4',
  'Chrome >= 35',
  'Firefox >= 31',
  'Explorer >= 9',
  'iOS >= 7',
  'Opera >= 12',
  'Safari >= 7.1'
];

const WHITE_LIST = [
  'font', 'font-size', 'line-height', 'letter-spacing',
  'padding', 'padding-top', 'padding-right', 'padding-bottom', 'padding-left',
  'margin', 'margin-top', 'margin-right', 'margin-bottom', 'margin-left',
  'top', 'right', 'bottom', 'left'
];

const WEBPACK_PLUGINS = {
  '_': 'lodash',
  '$': 'jquery',
  'jQuery': 'jquery',
  'axios': 'axios',
  'Tools': 'Tools',
  'React': 'react',
  'alt': 'altInstance',
  'Promise': 'bluebird',
  'ReactDOM': 'react-dom'
};

const WEBPACK_RESOLVE_EXTENSIONS = ['', '.react.js', '.js', '.jsx', '.css'];

const WEBPACK_RESOLVE_MODULES_DIR = ["app", "node_modules", "libs"];

module.exports = {
  AUTOPREFIXER_BROWSERS: AUTOPREFIXER_BROWSERS,
  WHITE_LIST: WHITE_LIST,
  WEBPACK_PLUGINS: WEBPACK_PLUGINS,
  WEBPACK_RESOLVE_EXTENSIONS: WEBPACK_RESOLVE_EXTENSIONS,
  WEBPACK_RESOLVE_MODULES_DIR: WEBPACK_RESOLVE_MODULES_DIR
};