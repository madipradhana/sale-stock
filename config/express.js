var express = require('express');
var enforce = require('express-sslify');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compress = require('compression');
var httpProxy = require('http-proxy');
var http = require('http');
var path = require('path');

var proxy = httpProxy.createProxyServer({});

var cacheAge = 31536000000;

module.exports = function (app) {
  "use strict";

  app.set('port', process.env.PORT);
  app.disable('x-powered-by');
  app.set('views', path.join(__dirname, '..', process.env.VIEWS_PATH));
  app.set('view engine', 'html');
  app.set('view cache', true);

  app.use(compress());

  app.use(express.static(path.join(__dirname, '..', '/build'), { maxAge: cacheAge }));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended: true})); // for parsing application/x-www-form-urlencoded
  app.use(cookieParser());

  // Webpack
  if (process.env.NODE_ENV === 'development') {
    var webpack = require('../webpack');
    webpack();

    app.all('/assets/*', function(req, res) {
      proxy.web(req, res, {
        target: 'http://localhost:8001'
      });
    });
  }
};
