var webpack = require('webpack');
var webpackDevServer = require('webpack-dev-server');
var webpackConfig = require('./config/webpack.config.dev.js');

module.exports = function() {
  var server = null;
  var config = webpack(webpackConfig);

  var server = new webpackDevServer(config, {
    publicPath: '/assets/',
    hot: true,
    quiet: false,
    noInfo: false,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Headers": "X-Requested-With"
    },
    stats: { colors: true },
    historyApiFallback: true
  });

  server.listen(8001, 'localhost', function () {
    console.log('Bundling project...');
  });
};